FROM debian:jessie
MAINTAINER Christiaan Kras <c.kras@pcc-online.net>

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get -yq install \
        curl \
        apache2 && \
    	rm -rf /var/lib/apt/lists/*

RUN a2enmod proxy_http proxy_ajp proxy_balancer proxy_wstunnel rewrite headers

RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf
ADD vhost-default.conf /etc/apache2/sites-available/001-default.conf
RUN a2dissite 000-default
RUN a2ensite 001-default

RUN mkdir /app /conf /var/lock/apache2
ADD sample/ /app
ADD proxy.conf /conf/



EXPOSE 80
WORKDIR /app
ADD run.sh /run.sh
RUN chmod 755 /*.sh
CMD ["/run.sh"]
