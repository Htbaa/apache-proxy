# Apache Reverse Proxy

 This docker image contains an Apache Httpd Server with proxy enabled, actually following modules are enabled : 

 * mod_proxy
   * proxy_http
   * proxy_ajp 
   * proxy_balancer
   * proxy_wstunnel

 * rewrite
 * headers

 Then the container contains two directries : 

 * **/app** : this directory will be the *DocumentRoot* of the Apache Web Server, if you've planed to use this container for proxy use only you may not use this directory.
 * **/conf** : All files matching **.conf* in this directory will be included in the vhost configuration (in the beginning of the vhost file)

## Example : 

 Imagine you are building a Wordpress site, you may have to use a container for Wordpress execution, that use a MySql Database (in another container), and for administering the database you want to use a phpmyadmin (in a container), so you will have at leat 3 containers : 

 * wordpress
 * mysql
 * phpmyadmin

 For executing all those containers you may use command like that :

```
docker run -d --name mysql mysql-container-image
docker run -d -p 8080:80 --link mysql:mysql --name phpmyadmin phpmyadmin-container-image 
docker run -d -p 80:80 --link mysql:mysql --name wordpress wordpress-docker-image
```

The *"Apache Reverse Proxy"* container will allow you to put all those containers behind a proxy. In order to do that, you will have to create a new docker image (with a new Dockerfile), create a *proxy.conf* file, and then build ***your*** proxy.

Dockerfile: 
```
FROM rgoyard/apache-proxy:latest
MAINTAINER Your Name <you@mail.com>
ADD proxy.conf /conf/
```

Then you need to create a **proxy.conf** file with the following lines

```
ProxyPass / http://wordpress/
ProxyPass /phpmyadmin http://phpmyadmin/
```

And you are ready to build and run your proxy 

```
docker build -t company/project-name:version .  
```

Now run it :


```
docker run -d --name mysql mysql-container-image
docker run -d --link mysql:mysql --name phpmyadmin phpmyadmin-container-image 
docker run -d --link mysql:mysql --name wordpress wordpress-docker-image
docker run -d -p 80:80 --link wordpress:wordpress --link phpmyadmin:phpmyadmin --name myProject company/project-name:version
```

That's it !

> **Note** : There is no need anymore to expose the "real" port of wordpress and phpmyadmin, as they are accessed from a reverse proxy

Of course you can use docker-compose (or fig) to create a full configuration file ...

This is a fork of rgoyard/apache-proxy.
